## ScanReader API

#### Create .env file in root project dir (from env.example):

    cp .env.example .env

#### Create database:

Exec in console from current environment:

    envsubst < tools/sql/create_db.sql | psql

Else read variables from .env file:

    (source .env && eval "echo \"$(cat tools/sql/create_db.sql)\"") | psql


#### Install Tesseract

    sudo apt-get install tesseract-ocr

#### Install language package:

    sudo apt-get install tesseract-ocr-[lang]

#### or install all language:

    sudo apt-get install tesseract-ocr-all
