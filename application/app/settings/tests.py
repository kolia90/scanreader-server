DEFAULT_FILE_STORAGE = 'inmemorystorage.InMemoryStorage'

MOMMY_CUSTOM_FIELDS_GEN = {}

CELERY_TASK_ALWAYS_EAGER = True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
