from .common import *

DEBUG = True

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'scan-reader-cache',
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
