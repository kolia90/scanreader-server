from model_mommy.recipe import Recipe

from app.pkg.main.models import Device, Scan, ScanItem, Document

DeviceRecipe = Recipe(Device)
ScanRecipe = Recipe(Scan)
ScanItemRecipe = Recipe(ScanItem)
DocumentRecipe = Recipe(Document)
