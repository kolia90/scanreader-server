from model_mommy.random_gen import gen_image_field
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from app.pkg.main.choices import Language, Output
from app.tests.test_main.utils import DeviceRecipe


class RecognizeTestCase(APITestCase):
    api_url = reverse('api:recognize')

    def setUp(self):
        self.device = DeviceRecipe.make()
        self.client.credentials(HTTP_AUTHORIZATION='Device %s' % self.device.device_id)

    def test_ocr_success(self):
        r = self.client.post(self.api_url, data={
            'lang': Language.ENG,
            'output': Output.TXT,
            'images': [gen_image_field(), gen_image_field()]
        })
        self.assertEqual(r.status_code, status.HTTP_201_CREATED, r.json())

    def test_ocr_failed_without_lang(self):
        r = self.client.post(self.api_url, data={
            'output': Output.TXT,
            'images': [gen_image_field()]
        })
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST, r.json())

    def test_ocr_failed_without_output(self):
        r = self.client.post(self.api_url, data={
            'lang': Language.ENG,
            'images': [gen_image_field()]
        })
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST, r.json())

    def test_ocr_failed_with_empty_images(self):
        r = self.client.post(self.api_url, data={
            'lang': Language.ENG,
            'output': Output.TXT,
            'images': []
        })
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST, r.json())
