from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from app.tests.test_main.utils import DeviceRecipe, DocumentRecipe


class DocumentsTestCase(APITestCase):
    api_url = reverse('api:documents')

    def setUp(self):
        self.device = DeviceRecipe.make()
        self.documents = DocumentRecipe.make(device=self.device, _quantity=3)
        self.other_documents = DocumentRecipe.make(_quantity=2)

        self.client.credentials(HTTP_AUTHORIZATION='Device %s' % self.device.device_id)

    def test_list_success(self):
        r = self.client.get(self.api_url)
        self.assertEqual(r.status_code, status.HTTP_200_OK, r.json())
        self.assertEqual(r.json().get('count'), len(self.documents))

    def test_retrieve_success(self):
        r = self.client.get(reverse('api:documents', args=[self.documents[0].pk]))
        self.assertEqual(r.status_code, status.HTTP_200_OK, r.json())

    def test_retrieve_failed_if_no_owner(self):
        r = self.client.get(reverse('api:documents', args=[self.other_documents[0].pk]))
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND, r.json())
