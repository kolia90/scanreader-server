from unittest import mock
from django.db import transaction


class FixCommitMixin:
    def run_commit_hooks(self):
        """
        Fake transaction commit to run delayed on_commit functions
        :return:
        """
        mock_path = 'django.db.backends.base.base.BaseDatabaseWrapper.validate_no_atomic_block'
        for db_name in reversed(self._databases_names()):
            with mock.patch(mock_path, lambda a: False):
                transaction.get_connection(using=db_name).run_and_clear_commit_hooks()
