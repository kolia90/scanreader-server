from django.conf import settings


class AuthenticationSwaggerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        user = getattr(request, 'user', None)
        if user and user.is_authenticated and request.path == settings.SWAGGER_DOC_PATH and settings.SWAGGER_AUTH_DEVICE_ID:
            request.META.update({
                'HTTP_AUTHORIZATION': 'Device %s' % settings.SWAGGER_AUTH_DEVICE_ID
            })

        return self.get_response(request)
