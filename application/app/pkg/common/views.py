from django.views.generic import TemplateView

from app.pkg.main.emails.notifications import DocumentNotify, FeedbackNotify


class IndexView(TemplateView):
    template_name = 'site/index.html'


class PrivacyView(TemplateView):
    template_name = 'site/privacy.html'


class PreviewDocumentView(TemplateView):
    template_name = 'emails/document/send_to_email.html'

    def get_context_data(self, **kwargs):
        message = DocumentNotify.get_message(name='My scan')
        kwargs.update(message.get_context_data())
        return super(PreviewDocumentView, self).get_context_data(**kwargs)


class PreviewFeedbackView(TemplateView):
    template_name = 'emails/management/feedback.html'

    def get_context_data(self, **kwargs):
        message = FeedbackNotify.get_message(
            email='test@domain.com', message='I have a new bug.'
        )
        kwargs.update(message.get_context_data())
        return super(PreviewFeedbackView, self).get_context_data(**kwargs)
