from django.urls import path

from app.pkg.common import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('privacy-policy', views.PrivacyView.as_view(), name='privacy'),
    path('preview/document', views.PreviewDocumentView.as_view(), name='preview-document'),
    path('preview/feedback', views.PreviewFeedbackView.as_view(), name='preview-feedback')
]
