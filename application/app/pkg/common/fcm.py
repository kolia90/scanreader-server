from django.conf import settings
from pyfcm import FCMNotification


push_service = FCMNotification(api_key=settings.FCM_API_KEY)
