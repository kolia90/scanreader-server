from rest_framework import exceptions, authentication
from django.utils.translation import ugettext_lazy as _

from app.pkg.main.models import Device


class DeviceAuthentication(authentication.BaseAuthentication):
    """
    Simple token based authentication.

    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Device ".  For example:

        Authorization: Device 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Device'

    def authenticate(self, request):
        auth = authentication.get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        try:
            device = Device.objects.get(device_id=key)
        except Device.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        return device, None

    def authenticate_header(self, request):
        return self.keyword
