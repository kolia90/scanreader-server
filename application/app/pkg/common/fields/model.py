import os
import json
import datetime
from functools import partial

from psycopg2.extras import Json
from pytils.translit import slugify
from django.db import models
from django.core import exceptions
from django.contrib.postgres.fields import JSONField
from django.core.serializers.json import DjangoJSONEncoder
from django.utils.encoding import force_str, force_text


class JSONDjangoEncoderField(JSONField):
    """
    Json field with DjangoJSONEncoder
    """
    django_encoder_dumps = partial(json.dumps, cls=DjangoJSONEncoder)

    def from_db_value(self, value, *args, **kwargs):
        if isinstance(value, str):
            try:
                return json.loads(value)
            except ValueError:
                return value
        return value

    def get_prep_value(self, value):
        if value is not None:
            return Json(value, dumps=self.django_encoder_dumps)
        return value

    def validate(self, value, model_instance):
        super(JSONField, self).validate(value, model_instance)
        try:
            self.django_encoder_dumps(value)
        except TypeError:
            raise exceptions.ValidationError(
                self.error_messages['invalid'],
                code='invalid',
                params={'value': value},
            )


class FileFieldSlugify(models.FileField):
    """
    File field with slugify filename when upload and remove old file
    """
    def __init__(self, verbose_name=None, upload_to='', **kwargs):
        self.upload_to_path = upload_to
        kwargs.update({
            'upload_to': partial(self.upload_filename, path=upload_to)
        })
        super(FileFieldSlugify, self).__init__(
            verbose_name=verbose_name, **kwargs)

    @staticmethod
    def upload_filename(instance, filename, path=''):
        f_name, dot, extension = filename.rpartition('.')
        norm_path = os.path.normpath(
            force_text(datetime.datetime.now().strftime(force_str(path))))
        filename = '{}.{}'.format(slugify(f_name), extension)
        return os.path.join(norm_path, filename)

    def deconstruct(self):
        name, path, args, kwargs = super(FileFieldSlugify, self).deconstruct()
        kwargs['upload_to'] = self.upload_to_path
        return name, path, args, kwargs


class ImageFieldSlugify(FileFieldSlugify, models.ImageField):
    """
    Image field with slugify filename when upload and remove old file
    """
