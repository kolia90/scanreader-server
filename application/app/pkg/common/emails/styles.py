
COMMON_CSS_STYLES = dict(
    body='''
        padding: 0;
        margin: 0;
        color: #333333;
        font-family: Arial, sans-serif;
    ''',
    wrap='''
        background: #ececec;
        color: #333;
    ''',
    header='''
        background: #005271;
        padding: 20px 30px;
        text-align: center;
    ''',
    logo='''
        height: 100px;
    ''',
    top='''

    ''',
    head_image='''
        width: 100%;
        opacity: 0.8;
    ''',
    content='''
        text-align: center;
        padding: 30px;
    ''',
    message='''

    ''',
    action='''
        text-align: center;
        padding: 0 30px 50px;
    ''',
    footer='''
        padding: 0 20px 10px;
        font-size: 12px;
        text-align: center;
        color: #777;
    ''',
    title='''
        margin-top: 0;
        margin-bottom: 20px;
        font-size: 16pt;
        font-weight: bold;
        color: #000;
    ''',
    link='''
        color: #1A90E2;
    ''',
    p='''
        margin-bottom: 10px;
        font-size: 11pt;
    ''',
    text_center='''
        text-align: center;
    ''',
)


def from_common_styles(**styles):
    merged = COMMON_CSS_STYLES
    merged.update(styles)
    return merged
