from django.db import transaction
from django.db.models.signals import post_save
from fieldsignals import post_save_changed
from django.dispatch import receiver

from app.pkg.common.fcm import push_service
from app.pkg.main.models import Document
from app.pkg.main.tasks import process_recognize


@receiver(post_save, sender=Document)
def start_process_document(instance, created, **kwargs):
    created and transaction.on_commit(lambda: process_recognize.delay(instance.pk))


@receiver(post_save_changed, sender=Document, fields=['status'])
def notify_about_status_changed(instance, **kwargs):
    if instance.is_completed:
        push_service.notify_single_device(registration_id=instance.device.token, data_message={
            'event': 'recognize.{}'.format(instance.status),
            'extra': {
                'id': instance.pk
            }
        })
