import logging
from datetime import timedelta

from constance import config
from django.utils import timezone
from celery.task import periodic_task

from app.celery import app
from app.pkg.main.helpers.recognize import recognize_document
from app.pkg.main.models import Document, Scan

logger = logging.getLogger('recognize')


@app.task
def process_recognize(doc_id):
    try:
        document = Document.objects.get(pk=doc_id)
    except Document.DoesNotExist:
        return

    document.mark_as_processing()
    try:
        recognize_document(document)
    except Exception as e:
        document.mark_as_error()
        logger.error(e.__str__())
    else:
        document.mark_as_finished()


@periodic_task(run_every=timedelta(minutes=15))
def delete_expired_documents():
    if not config.CLEAR_TASK_ENABLED:
        return

    now = timezone.now()
    check_date = now - timedelta(hours=config.CLEAR_FILE_AFTER_HOURS)
    Scan.objects.filter(created__lte=check_date).delete()
