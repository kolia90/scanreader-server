import os
import tempfile
import pytesseract
from io import BytesIO
from docx import Document
from PIL import Image
from PIL import TiffImagePlugin
from django.core.files.base import ContentFile


def recognize_document(document):
    items = document.source.items.order_by('order')
    if document.is_output_pdf:
        fn = tempfile.mktemp('.tiff')
        with TiffImagePlugin.AppendingTiffWriter(fn, True) as tf:
            for item in items:
                im = Image.open(item.image.path)
                im.save(tf)
                tf.newFrame()
                im.close()

            pdf = pytesseract.image_to_pdf_or_hocr(fn, extension='pdf', lang=document.lang)
            document.file.save('doc-%s.pdf' % document.pk, ContentFile(pdf))
            os.remove(fn)

    elif document.is_output_txt:
        data = ''
        for item in items:
            data += pytesseract.image_to_string(item.image.path, lang=document.lang)

        document.file.save('doc-%s.txt' % document.pk, ContentFile(data))

    elif document.is_output_docx:
        doc = Document()
        f = BytesIO()

        for item in items:
            doc.add_paragraph(pytesseract.image_to_string(item.image.path, lang=document.lang))
        doc.save(f)

        f.seek(0)
        document.file.save('doc-%s.docx' % document.pk, ContentFile(f.read()))
