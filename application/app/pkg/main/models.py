from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from app.pkg.common.fields.model import (
    JSONDjangoEncoderField, FileFieldSlugify, ImageFieldSlugify
)
from app.pkg.main.choices import Language, Output, DocumentStatus
from app.pkg.main.managers import (
    ScanManager, ScanItemManager, DocumentManager
)


class Device(models.Model):
    device_id = models.CharField(
        verbose_name=_('device ID'), db_index=True,
        help_text=_('unique device identifier'),
        max_length=150
    )
    token = models.TextField(verbose_name=_('registration token'))
    created = models.DateTimeField(_('created'), auto_now_add=True)
    updated = models.DateTimeField(_('updated'), auto_now=True)

    class Meta:
        verbose_name = _('Device')
        verbose_name_plural = _('Devices')

    def __str__(self):
        return self.device_id

    @property
    def is_authenticated(self):
        return True


class Scan(models.Model):
    device = models.ForeignKey(Device, verbose_name=_('device'), related_name='scans', on_delete=models.CASCADE)
    name = models.CharField(_('name'), max_length=255, null=True, blank=True)
    is_shared = models.BooleanField(_('shared'), default=False)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    objects = ScanManager()

    class Meta:
        verbose_name = _('Scan')
        verbose_name_plural = _('Scans')

    def __str__(self):
        return _('Scan ID: %s') % self.id

    @property
    def share_url(self):
        return reverse('')


class ScanItem(models.Model):
    scan = models.ForeignKey(Scan, verbose_name=_('scan'), related_name='items', on_delete=models.CASCADE)
    image = ImageFieldSlugify(verbose_name=_('image'), upload_to='scans/%Y/%m/%d/')
    order = models.PositiveIntegerField(_('order'), default=0)
    created = models.DateTimeField(_('created'), auto_now_add=True)

    objects = ScanItemManager()

    class Meta:
        verbose_name = _('OCR')
        verbose_name_plural = _('OCRs')

    def __str__(self):
        return _('Scan item ID: %s') % self.id

    def delete(self, using=None, keep_parents=False):
        self.image.delete()
        return super(ScanItem, self).delete(using, keep_parents)


class Document(models.Model):
    device = models.ForeignKey(Device, verbose_name=_('device'), related_name='documents', on_delete=models.CASCADE)
    source = models.ForeignKey(Scan, verbose_name=_('scan'), related_name='documents', on_delete=models.CASCADE)
    file = FileFieldSlugify(verbose_name=_('file'), upload_to='documents/%Y/%m/%d/')
    lang = models.CharField(_('language'), choices=Language.choices(), max_length=10)
    output = models.CharField(_('output'), choices=Output.choices(), max_length=20)
    trim = models.BooleanField(_('delete spec chars'), default=False)
    is_shared = models.BooleanField(_('shared'), default=False)
    created = models.DateTimeField(_('created'), auto_now_add=True)
    status = models.CharField(_('status'), max_length=50,
                              choices=DocumentStatus.choices(), default=DocumentStatus.CREATED)
    extra = JSONDjangoEncoderField(_('extra data'), null=True, editable=False)

    objects = DocumentManager()

    class Meta:
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')
        unique_together = ('source', 'lang')

    def __str__(self):
        return _('Document ID: %s') % self.id

    def delete(self, using=None, keep_parents=False):
        self.file.delete()
        return super(Document, self).delete(using, keep_parents)

    @property
    def share_url(self):
        return reverse('')

    def change_status(self, status):
        self.status = status
        self.save()

    def mark_as_processing(self):
        self.change_status(DocumentStatus.PROCESSING)

    def mark_as_finished(self):
        self.change_status(DocumentStatus.FINISHED)

    def mark_as_error(self):
        self.change_status(DocumentStatus.ERROR)

    @property
    def is_status_created(self):
        return self.status == DocumentStatus.CREATED

    @property
    def is_status_processing(self):
        return self.status == DocumentStatus.PROCESS

    @property
    def is_status_finished(self):
        return self.status == DocumentStatus.FINISHED

    @property
    def is_status_error(self):
        return self.status == DocumentStatus.ERROR

    @property
    def is_completed(self):
        return self.is_status_finished or self.is_status_error

    @property
    def is_output_txt(self):
        return self.output == Output.TXT

    @property
    def is_output_docx(self):
        return self.output == Output.DOCX

    @property
    def is_output_pdf(self):
        return self.output == Output.PDF
