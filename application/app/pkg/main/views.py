from rest_framework import generics

from app.pkg.common.permissions import HasAPIAccess
from app.pkg.main.serializers import (
    DeviceSerializer, RecognizeSerializer, DocumentSerializer,
    SendToEmailSerializer, FeedbackSerializer
)


class DeviceView(generics.CreateAPIView):
    serializer_class = DeviceSerializer
    permission_classes = (HasAPIAccess,)


class RecognizeView(generics.CreateAPIView):
    serializer_class = RecognizeSerializer


class SendToEmailView(generics.CreateAPIView):
    serializer_class = SendToEmailSerializer


class DocumentListView(generics.ListAPIView):
    serializer_class = DocumentSerializer
    queryset = DocumentSerializer.Meta.model.objects

    def get_queryset(self):
        return self.queryset.filter(device=self.request.user)


class DocumentRetrieveView(generics.RetrieveAPIView):
    serializer_class = DocumentSerializer
    queryset = DocumentSerializer.Meta.model.objects

    def get_queryset(self):
        return self.queryset.filter(device=self.request.user)


class FeedbackView(generics.CreateAPIView):
    serializer_class = FeedbackSerializer
