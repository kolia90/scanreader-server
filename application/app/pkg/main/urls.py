from django.urls import path

from app.pkg.main import views

urlpatterns = [
    path('device', views.DeviceView.as_view(), name='device'),
    path('recognize', views.RecognizeView.as_view(), name='recognize'),
    path('send_to_email', views.SendToEmailView.as_view(), name='send_to_email'),
    path('feedback', views.FeedbackView.as_view(), name='feedback'),

    path('documents', views.DocumentListView.as_view(), name='documents'),
    path('documents/<int:pk>', views.DocumentRetrieveView.as_view(), name='documents'),
]
