from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'app.pkg.main'

    def ready(self):
        import app.pkg.main.signal_handlers
