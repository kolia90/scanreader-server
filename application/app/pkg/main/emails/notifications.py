from django.utils.translation import ugettext_lazy as _

from app.pkg.common.emails.service import EmailNotification


class DocumentNotify(EmailNotification):
    template_name = 'emails/document/send_to_email.html'
    subject = _('Document "%(name)s"')


class FeedbackNotify(EmailNotification):
    template_name = 'emails/management/feedback.html'
    subject = _('New feedback from "%(email)s"')
