from django.db.models import QuerySet, Manager


class ScanQuerySet(QuerySet):
    def delete(self):
        for item in self._chain():
            item.items.all().delete()
            item.documents.all().delete()
        return super(ScanQuerySet, self).delete()


class ScanManager(Manager):
    _queryset_class = ScanQuerySet


class ScanItemQuerySet(QuerySet):

    def delete(self):
        for obj in self:
            obj.image.delete()
        return super(ScanItemQuerySet, self).delete()


class ScanItemManager(Manager):
    _queryset_class = ScanItemQuerySet


class DocumentQuerySet(QuerySet):

    def delete(self):
        for obj in self:
            obj.file.delete()
        return super(DocumentQuerySet, self).delete()


class DocumentManager(Manager):
    _queryset_class = DocumentQuerySet
