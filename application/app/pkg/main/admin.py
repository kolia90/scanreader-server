from django.contrib import admin

from app.pkg.main.models import Device, Scan, ScanItem, Document


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'created', 'updated')
    search_fields = ('device_id',)


@admin.register(Scan)
class ScanAdmin(admin.ModelAdmin):

    class ScanItemInline(admin.StackedInline):
        model = ScanItem
        exclude = ()
        readonly_fields = ('order',)
        extra = 0
        max_num = 0

    list_display = ('__str__', 'name', 'device', 'is_shared', 'created')
    inlines = [ScanItemInline]
    raw_id_fields = ('device', )
    search_fields = ('device__device_id',)


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'source', 'lang', 'status', 'is_shared', 'created')
    raw_id_fields = ('device', 'source')
    search_fields = ('device__device_id',)
