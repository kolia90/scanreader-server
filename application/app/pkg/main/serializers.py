from django.conf import settings
from rest_framework import serializers
from django.core.cache import cache
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import Throttled

from app.pkg.main.choices import Language, Output
from app.pkg.main.emails.notifications import DocumentNotify, FeedbackNotify
from app.pkg.main.models import Device, Document, Scan, ScanItem


class DeviceSerializer(serializers.ModelSerializer):
    device_id = serializers.UUIDField()

    class Meta:
        model = Device
        exclude = ()

    def create(self, validated_data):
        device_id = validated_data.get('device_id')
        token = validated_data.get('token')

        device, _ = Device.objects.update_or_create(device_id=device_id, defaults={
            'token': token
        })
        return device


class ScanItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScanItem
        exclude = ('scan',)


class ScanSerializer(serializers.ModelSerializer):
    items = ScanItemSerializer(many=True)

    class Meta:
        model = Scan
        exclude = ('device',)


class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scan
        exclude = ('device',)


class BlockerMixin(object):

    def get_blocker_key(self, device):
        cache_key = self.__class__.__name__.lower().replace('serializer', '')
        return 'blocked-{}-{}'.format(cache_key, device.pk)

    def set_blocker(self, device, lifetime):
        cache.set(self.get_blocker_key(device), True, lifetime)

    def validate_device(self, device):
        if cache.get(self.get_blocker_key(device), None):
            raise Throttled(detail=_('You can not do it very often'))
        return device


class RecognizeSerializer(BlockerMixin, serializers.ModelSerializer):
    lang = serializers.ChoiceField(choices=Language.choices())
    name = serializers.CharField(allow_null=True, allow_blank=True, required=False)
    images = serializers.ListField(child=serializers.ImageField(), write_only=True, min_length=1)
    output = serializers.ChoiceField(choices=Output.choices())
    device = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    extra = serializers.CharField(required=False, allow_null=True)

    class Meta:
        model = Document
        exclude = ()
        read_only_fields = ('file', 'source', 'status')

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        device = validated_data.pop('device')
        name = validated_data.pop('name', None)
        images = validated_data.pop('images', [])

        scan = Scan.objects.create(device=device, name=name)
        for i, image in enumerate(images):
            ScanItem.objects.create(scan=scan, image=image, order=i)

        instance = Document.objects.create(**validated_data, source=scan, device=device)

        self.set_blocker(device, settings.EVENT_RECOGNIZE_BLOCK_SECONDS)
        return instance


class DocumentSerializer(serializers.ModelSerializer):
    source = ResourceSerializer()

    class Meta:
        model = Document
        exclude = ('device',)


class SendToEmailSerializer(BlockerMixin, serializers.Serializer):
    files = serializers.ListField(child=serializers.FileField(), write_only=True, min_length=1)
    name = serializers.CharField(required=False)
    email = serializers.EmailField()
    device = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        fields = '__all__'

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        name = validated_data.get('name')
        email = validated_data.get('email')
        device = validated_data.get('device')
        files = validated_data.get('files', None)

        self.set_blocker(device, settings.EVENT_EMAIL_BLOCK_SECONDS)
        DocumentNotify.send(email, name=name, attachments=files)
        return validated_data


class FeedbackSerializer(BlockerMixin, serializers.Serializer):
    email = serializers.EmailField()
    message = serializers.CharField()
    device = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        fields = '__all__'

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        email = validated_data.get('email')
        device = validated_data.get('device')
        message = validated_data.get('message', None)

        self.set_blocker(device, settings.EVENT_FEEDBACK_BLOCK_SECONDS)
        FeedbackNotify.send_to_admin(email=email, message=message)
        return validated_data
